#pragma once

#include <QMainWindow>
#include <memory>

struct Hybridation3D_TWI_Root;

namespace Ui {
class MainWindow;
}

struct MainWindow : public QMainWindow{
	MainWindow(QWidget *parent = nullptr);
	~MainWindow();
private:
	Ui::MainWindow* ui;
};
