#include "mainwindow.h"
#include "./ui_mainwindow.h"

#include "QCursor"


#include <iostream>
#include <algorithm>
#include <map>


struct Scene{
	struct Named{
		std::string name;
		bool operator ==(const Named& other)const{return other.name==name;}
	};


	struct Camera:public Named{};
	struct Fond:public Named{};
	struct Atmosphere{};
	struct Renderer{};
	struct Light:public Named{};
	struct Object:public Named{};
	struct Result:public Named{};
	struct TargetingAssosation:public Named{};


	Camera cam;
	Fond bkg;
	Atmosphere atm;
	Renderer rendere;
	std::vector<Light> lights;
	std::vector<Object> objects;
	std::vector<Result> results;
	std::vector<TargetingAssosation> targetings;
};


struct Hybridation3D_TWI_base: public QTreeWidgetItem{
	Hybridation3D_TWI_base(Scene& scene):scene(scene){}
	virtual void showContextMenu()=0;
	virtual void updateFromModel()=0;

	void removeAllChilds(){
		while (childCount()) {
			removeChild(child(0));
		}
	}

	void updateParent(){
		if(auto p=dynamic_cast<Hybridation3D_TWI_base*>(parent())){
			p->updateFromModel();
		}
	}

protected:
	Scene &scene;
};


template<typename T>
struct Hybridation3D_TWI_Item{
	Hybridation3D_TWI_Item() = delete;
};


template<>
struct Hybridation3D_TWI_Item<Scene::Light>: public Hybridation3D_TWI_base{
	Hybridation3D_TWI_Item(Scene::Light& sceneItem,Scene& scene):Hybridation3D_TWI_base(scene),sceneItem(sceneItem){}

	virtual void updateFromModel() override{};

	void showContextMenu() override{
		QMenu menu;
		menu.addAction("Suppr",[this](){
			auto it=std::find(scene.lights.begin(),scene.lights.end(),sceneItem);
			if(it!=scene.lights.end()){
				scene.lights.erase(it);
				updateParent();
			}
		});

		menu.exec(QCursor::pos());
	}

	QVariant data(int column, int role) const override{
		switch (role) {
		case Qt::ItemDataRole::DisplayRole:{
			return "aze"+QString::fromStdString(sceneItem.name);
		}break;
		}

		return QVariant();
	}
private:
	Scene::Light& sceneItem;
};

template<>
struct Hybridation3D_TWI_Item<Scene::Object>: public Hybridation3D_TWI_base{
	Hybridation3D_TWI_Item(Scene::Object& sceneItem,Scene& scene):Hybridation3D_TWI_base(scene){}

	virtual void updateFromModel() override{};

	void showContextMenu() override{
		std::cout << "coucou Object" << std::endl;
	}

	QVariant data(int column, int role) const override{
		switch (role) {
		case Qt::ItemDataRole::DisplayRole:{
			return "Item (" + QString::number(childCount())+ ")";
		}break;
		}

		return QVariant();
	}
};

template<>
struct Hybridation3D_TWI_Item<Scene::Result>: public Hybridation3D_TWI_base{
	Hybridation3D_TWI_Item(Scene::Result& sceneItem,Scene& scene):Hybridation3D_TWI_base(scene){}

	virtual void updateFromModel() override{};

	void showContextMenu() override{
		std::cout << "coucou Result" << std::endl;
	}

	QVariant data(int column, int role) const override{
		switch (role) {
		case Qt::ItemDataRole::DisplayRole:{
			return "Item (" + QString::number(childCount())+ ")";
		}break;
		}

		return QVariant();
	}
};


template<typename T>
struct Hybridation3D_TWI_Folder: public Hybridation3D_TWI_base{
	std::vector<T>& sceneItems;
	QString name;

	Hybridation3D_TWI_Folder(std::vector<T>& sceneItems, std::string name, Scene& scene)
		:Hybridation3D_TWI_base(scene)
		,sceneItems(sceneItems)
		,name(QString::fromStdString(name))
	{
		updateFromModel();
	}

	void updateFromModel() override{
		removeAllChilds();

		for (auto& sceneItem : sceneItems) {
			addChild(new Hybridation3D_TWI_Item<T>(sceneItem,scene));
		}
	}

	void showContextMenu() override{
		QMenu menu;
		menu.addAction("vider",[this](){
			sceneItems.clear();
			updateFromModel();
		});

		menu.addAction("ajoute un enfant",[this](){
			sceneItems.push_back({});
			updateFromModel();
		});

		menu.exec(QCursor::pos());
	}

	QVariant data(int column, int role) const override{
		switch (role) {
		case Qt::ItemDataRole::DisplayRole:{
			return name+ " (" + QString::number(childCount())+ ")";
		}break;

		case Qt::ItemDataRole::DecorationRole:{
			return QWidget().style()->standardIcon(QStyle::SP_DirOpenIcon);
		}break;
		}

		return QVariant();
	}
};

struct Hybridation3D_TWI_Root: public Hybridation3D_TWI_base{
	Hybridation3D_TWI_Root(Scene& scene, QTreeWidget& tw):Hybridation3D_TWI_base(scene){
		tw.addTopLevelItem(this);
		updateFromModel();
	}

	void updateFromModel() override{
		removeAllChilds();

		addChild(new Hybridation3D_TWI_Folder<Scene::Light>(scene.lights,"Lights",scene));
		addChild(new Hybridation3D_TWI_Folder<Scene::Object>(scene.objects,"Object",scene));
		addChild(new Hybridation3D_TWI_Folder<Scene::Result>(scene.results,"Result",scene));
	}


	void showContextMenu() override{}

	QVariant data(int column, int role) const override{
		switch (role) {
		case Qt::ItemDataRole::DisplayRole:{
			return "ROOT";
		}break;

		case Qt::ItemDataRole::DecorationRole:{
			return QWidget().style()->standardIcon(QStyle::SP_DriveCDIcon);
		}break;


		}

		return QVariant();
	}

};



/*
root (scene)
	camera
	fond: Union[2d,hdri]
	Atmosphere
	rederer
	lumieres
		lumiere
	assosiations de visées
	objets
		objet3D
	results
		resultat
*/

Scene scene=[](){
	Scene ret;
	ret.lights.push_back({});ret.lights.back().name="1";
	ret.lights.push_back({});ret.lights.back().name="2";
	ret.lights.push_back({});ret.lights.back().name="63";
	ret.lights.push_back({});ret.lights.back().name="4";
	return ret;
}();


MainWindow::MainWindow(QWidget *parent)
	: QMainWindow(parent)
	, ui(new Ui::MainWindow)
{
	ui->setupUi(this);

	new Hybridation3D_TWI_Root(scene,*ui->tw);

	ui->tw->setContextMenuPolicy(Qt::CustomContextMenu);
	connect(ui->tw,&QTreeWidget::customContextMenuRequested,[this](const QPoint &pos){
		if(auto item=dynamic_cast<Hybridation3D_TWI_base*>(ui->tw->itemAt(pos))){
			item->showContextMenu();
		}
	});
}

MainWindow::~MainWindow(){
	delete ui;
}
